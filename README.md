# Gitlab Runner

## Register Runner
To register your Gitlab runner with Gitlab follow this tutorial https://dockerswarm.rocks/gitlab-ci/. Here's a short version of that tutorial.

Run the following to start the Gitlab runner docker container:
```sh
docker run -d \
    --name gitlab-runner \
    --restart always \
    -v gitlab-runner:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner:latest
```

Enter the container by running:
```sh
docker exec -it gitlab-runner bash
```

Open your Gitlab project and under "Settings" click on "CI / CD" then expand "Runners". Copy the url and token shown under "Set up a specific Runner manually" and enter them in the commands below and execute them inside your gitlab-runner container like this:

```sh
export GITLAB_URL=https://gitlab.com/
export GITLAB_TOKEN=WYasdfJp4sdfasdf1234
```

Then run the following command to register your Gitlab runner with Gitlab:
```sh
gitlab-runner \
    register -n \
    --name "Docker Runner" \
    --executor docker \
    --docker-image docker:latest \
    --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
    --url $GITLAB_URL \
    --registration-token $GITLAB_TOKEN \
    --tag-list dog-cat-cluster,stag,prod
```

If you now refresh the page (Gitlab project -> Settings -> CI / CD -> Runners) in Gitlab you should see your runner with the name "Docker Runner" show up as online.

## Use configuration

If you have registered a runner and want to save it as a configuration you can run the steps above and then go into Gitlab and in your Gitlab project -> Settings -> CI / CD -> Runners and beside your GItlab runner click on the "edit" icon then copy the Token. Next open config.toml in this project and replace YOUR_GITLAB_RUNNER_TOKEN in the file with the token from your Gitlab runner. Also replace the url with your own url if you are hosting your own Gitlab.

Next you can run "docker stack deploy --compose-file docker-compose.yml gitlab-runner" to deploy your Gitlab runner (make sure that you have stopped the docker container above before you start a new one). 

## Good to know

If you want to reuse your Gitlab runner in multiple projects then go to your Gitlab project -> Settings -> CI / CD -> Runners and beside your Gitlab runner click on the "edit" icon. Then on the new page uncheck the option "Lock to current projects".

Remember to disable shared runners if you only want your own Gitlab runner to execute jobs. You can do this by going to Gitlab project -> Settings -> CI / CD -> Runners and click on "Disable shared Runners".
