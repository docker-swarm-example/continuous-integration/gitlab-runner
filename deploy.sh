#!/bin/bash

export CI_COMMIT_SHA=$(git rev-parse HEAD)

if grep -q YOUR_GITLAB_RUNNER_TOKEN "config.toml"; then
    echo "Replace YOUR_GITLAB_RUNNER_TOKEN with your Gitlab runner token in config.toml file before deploying."
    exit 1;
fi

docker stack deploy --compose-file docker-compose.yml "continuous-integration"
